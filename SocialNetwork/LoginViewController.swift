//
//  LoginViewController.swift
//  SocialNetwork
//
//  Created by Aluno-r17 on 09/07/15.
//  Copyright (c) 2015 Unifor. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var senhaField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        //self.navigationController?.navigationBarHidden = true
        // Layout da barrinha do topo do iphone
        UIApplication.sharedApplication().statusBarStyle = UIStatusBarStyle.LightContent
        
    }
    
    // Barrinha de navegacao la em cima
    override func viewWillAppear(animated: Bool) {
        self.navigationController?.navigationBarHidden = true
    }
    override func viewWillDisappear(animated: Bool) {
        self.navigationController?.navigationBarHidden = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    @IBAction func botaoEntrar(sender: AnyObject) {
        var alert = UIAlertView()
        
        if !isValidEmail(self.emailField.text) {
            alert.title = "ERRO"
            alert.message = "E-mail invalido"
            alert.addButtonWithTitle("OK")
            alert.show()
            return
        }
        
        if (count(self.senhaField.text) < 6) {
            alert.title = "ERRO"
            alert.message = "A senha deve conter um minimo de 6 caracteres"
            alert.addButtonWithTitle("OK")
            alert.show()
            return
        }
        
        alert.title = "Teste"
        alert.message = "Teste"
        alert.addButtonWithTitle("OK")
        alert.show()
        
        // Isso faz ir pra tela do I like my headphone
        self.dismissViewControllerAnimated(true, completion: { () -> Void in
            //
        })
    }
    
    /*func validateEmail(candidate: String) -> Bool {
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
        let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegex)
        return emailTest.evaluateWithObject(candidate)
    }*/
    
    func isValidEmail(testStr:String) -> Bool {
        // println("validate calendar: \(testStr)")
        let emailRegEx = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluateWithObject(testStr)
    }
}
