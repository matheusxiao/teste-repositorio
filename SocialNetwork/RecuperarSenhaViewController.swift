//
//  RecuperarSenhaViewController.swift
//  SocialNetwork
//
//  Created by Aluno-r17 on 10/07/15.
//  Copyright (c) 2015 Unifor. All rights reserved.
//

import UIKit

class RecuperarSenhaViewController: UIViewController {

    @IBOutlet weak var emailTextF: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func botaoRecSenha(sender: AnyObject) {
        var alert = UIAlertView()
        
        if !isValidEmail(self.emailTextF.text) {
            alert.title = "ERRO"
            alert.message = "E-mail invalido"
            alert.addButtonWithTitle("OK")
            alert.show()
            return
        }
        
        // Conferir se email ja tem cadastro?
        // IF
        
        alert.title = "Teste"
        alert.message = "Teste"
        alert.addButtonWithTitle("OK")
        alert.show()
    }
    
    func isValidEmail(testStr:String) -> Bool {
        // println("validate calendar: \(testStr)")
        let emailRegEx = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluateWithObject(testStr)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
