//
//  FeedViewController.swift
//  SocialNetwork
//
//  Created by Aluno-r17 on 09/07/15.
//  Copyright (c) 2015 Unifor. All rights reserved.
//

import UIKit

class FeedViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    
    var reviews: [Review] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // identifier - classe
        var login = false
        
        if !login {
            self.performSegueWithIdentifier("login", sender: self)
        }
        
        // cor da navigation e tal
        self.navigationController!.navigationBar.barTintColor = UIColor(red: 61.0/255.0, green: 88.0/255.0, blue: 148.0/255.0, alpha: 1.0)
        self.navigationController!.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.whiteColor()]
        self.title = "I like my Headphone"
        
        // Coleta os reviews
        self.getReviews()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return self.reviews.count
    }
    
    // Row display. Implementers should *always* try to reuse cells by setting each cell's reuseIdentifier and querying for available reusable cells with dequeueReusableCellWithIdentifier:
    // Cell gets various attributes set automatically based on table (separators) and data source (accessory views, editing controls)
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cell: FeedTableViewCell = tableView.dequeueReusableCellWithIdentifier("feedCell", forIndexPath: indexPath) as!FeedTableViewCell
        
        var review: Review = self.reviews[indexPath.row]
        
        cell.marcaLabel.text = review.brand
        cell.commentLabel.text = review.comment
        cell.modelLabel.text = review.model
    
        // RATING
        
        var ratingname = "rating-\(review.rating!)-stars"
        cell.rating.image = UIImage(named: ratingname)
        
        return cell //??
    }
    
    // Metodo dos reviews
    func getReviews () {
        var review = Review()
        
        review.brand = "SolRepublic"
        review.model = "83754"
        review.rating = 4
        review.comment = "Massa!"
        review.picture = "htttp://"
        self.reviews.append(review)
        
        var review2 = Review()
        
        review2.brand = "Beats by DRE"
        review2.model = "SOLO HD"
        review2.rating = 5
        review2.comment = "Muito bom"
        review2.picture = "htttp://"
        self.reviews.append(review2)
        
        var review3 = Review()
        
        review3.brand = "Sony"
        review3.model = "AE3546"
        review3.rating = 4
        review3.comment = "Show"
        review3.picture = "http://"
        self.reviews.append(review3)
    
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
