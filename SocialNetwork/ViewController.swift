//
//  ViewController.swift
//  SocialNetwork
//
//  Created by Aluno-r17 on 08/07/15.
//  Copyright (c) 2015 Unifor. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var emailTextF: UITextField!
    
    @IBOutlet weak var senhaTextF: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func botaoEntrar(sender: AnyObject) {
        var alert = UIAlertView()
        
        if !validateEmail(self.emailTextF.text) {
            alert.title = "ERRO"
            alert.message = "E-mail invalido"
            alert.addButtonWithTitle("OK")
            alert.show()
            return
        }
        
        if count(self.senhaTextF.text) < 6 {
            alert.title = "ERRO"
            alert.message = "A senha deve conter um minimo de 6 caracteres"
            alert.addButtonWithTitle("OK")
            alert.show()
            return
        }
        
        alert.title = "Teste"
        alert.message = "Teste"
        alert.addButtonWithTitle("OK")
        alert.show()
        
    }
    
    func validateEmail(candidate: String) -> Bool {
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
        return NSPredicate(format: "SELF MATCHES %@", emailRegex).evaluateWithObject(candidate)
    }

}

