//
//  FeedTableViewCell.swift
//  SocialNetwork
//
//  Created by Aluno-r17 on 10/07/15.
//  Copyright (c) 2015 Unifor. All rights reserved.
//

import UIKit

class FeedTableViewCell: UITableViewCell {

    @IBOutlet weak var marcaLabel: UILabel!
    @IBOutlet weak var modelLabel: UILabel!
    @IBOutlet weak var commentLabel: UILabel!
    @IBOutlet weak var rating: UIImageView!
    @IBOutlet weak var userpic: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
